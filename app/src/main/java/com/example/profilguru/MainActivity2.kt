package com.example.profilguru

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val btn = findViewById<Button>(R.id.guru)
        val btn2 = findViewById<Button>(R.id.sekolah)

        btn.setOnClickListener {
            val Intent = Intent(this, MainActivity3::class.java)
            startActivity(Intent)

        }
        btn2.setOnClickListener {
            val Intent = Intent(this, MainActivity4::class.java)
            startActivity(Intent)

        }
    }
}